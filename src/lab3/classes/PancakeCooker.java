package lab3.classes;

public class PancakeCooker {
    private Dough dough;
    private Stove stove;
    private Tableware tableware;

    public PancakeCooker(Dough dough, Stove stove, Tableware tableware){
        this.dough = dough;
        this.stove = stove;
        this.tableware = tableware;
    }

    public void cookPancakes(long amount){
        tableware.prepareAllTools();
        dough.kneadDough();
        dough.prepareForCooking();
        stove.cook(amount);
        tableware.cleanAllTools();
    }
}
