package lab3;

import java.util.Scanner;

import lab3.classes.Dough;
import lab3.classes.PancakeCooker;
import lab3.classes.Stove;
import lab3.classes.Tableware;

public class Launch {
    public static void main(String[] args) {
        var dough = new Dough();
        var stove = new Stove();
        var tableware = new Tableware();

        var cooker = new PancakeCooker(dough, stove, tableware);

        var scanner = new Scanner(System.in);
        System.out.println("Сколько испечь блинов? ");
        cooker.cookPancakes(Long.parseLong(scanner.nextLine()));
    }
}
