package lab2.decorators;

import lab2.interfaces.Product;

public class DressingTopping extends ToppingDecorator {
    public DressingTopping(Product product) {
        super(50, "Dressing ", product);
    }   
}
