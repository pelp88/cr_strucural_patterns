package lab2.decorators;

import lab2.interfaces.Product;

public class SyrupTopping extends ToppingDecorator{
    public SyrupTopping(Product product) {
        super(100, "Syrup ", product);
    }
}