package lab2.decorators;

import lab2.interfaces.Product;

public class ToppingDecorator implements Product{
    public double price;
    public String name;
    public Product product;

    public ToppingDecorator(double price, String name, Product product) {
        this.product = product;
        this.name = name;
        this.price = price;
    }

    @Override
    public String getName() {
        return this.name + this.product.getName();
    }

    @Override
    public double getPrice() {
        return this.price + this.product.getPrice();
    }
}
