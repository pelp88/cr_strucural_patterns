package lab2;

import java.util.Scanner;

import lab2.classes.Icecream;
import lab2.classes.Smoothie;
import lab2.classes.Yoghurt;
import lab2.decorators.DressingTopping;
import lab2.decorators.SyrupTopping;
import lab2.interfaces.Product;

public class Launch {
    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        var price = 500;
        Product product;
        System.out.println("Enter product type (icecream, yoghurt, smoothie): ");
        switch (scanner.nextLine()) {
            case "icecream":
                product = toppingProcessor(new Icecream(price), scanner);
                break;
            case "yoghurt":
                product = toppingProcessor(new Yoghurt(price), scanner);
                break;
            case "smoothie":
                product = toppingProcessor(new Smoothie(price), scanner);
                break;
            default:
                product = toppingProcessor(new Icecream(price), scanner);
                break;
        }

        System.out.println("Final product: " + product.getName() + " - " + product.getPrice());
    }

    public static Product toppingProcessor(Product product, Scanner scanner) {
        System.out.println("Use toppings? (choose: dressing, syrup, both, none) (default is none)");
        switch (scanner.nextLine()) {
            case "dressing":
                return new DressingTopping(product);
            case "syrup":
                return new SyrupTopping(product);
            case "both":
                return new SyrupTopping(new DressingTopping(product));
            case "none":
                return product;
            default:
                return product;
        }
    }
}
