package lab2.classes;

import lab2.interfaces.Product;

public class Icecream implements Product {
    public double price;
    public String name = "Icecream";

    public Icecream(double price) {
        this.price = price;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getPrice() {
        return this.price;
    }
}
