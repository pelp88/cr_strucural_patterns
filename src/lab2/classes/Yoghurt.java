package lab2.classes;

import lab2.interfaces.Product;

public class Yoghurt implements Product {
    public double price;
    public String name = "Yoghurt";

    public Yoghurt(double price) {
        this.price = price;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getPrice() {
        return this.price;
    }
}
