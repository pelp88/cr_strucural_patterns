package lab2.classes;

import lab2.interfaces.Product;

public class Smoothie implements Product{
    public double price;
    public String name = "Smoothie";

    public Smoothie(double price) {
        this.price = price;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public double getPrice() {
        return this.price;
    }
}
