package lab1.adapter;

import lab1.roundpeg.RoundPeg;
import lab1.squarepeg.SquarePeg;

public class SquarePegAdapter extends RoundPeg{
    private SquarePeg squarePeg;

    public SquarePegAdapter(SquarePeg sPeg) {
        this.squarePeg = sPeg;
    }
    
    @Override
    public double getRadius() {
        System.out.println(Math.sqrt(2) * this.squarePeg.getWidth());
        return Math.sqrt(2) * this.squarePeg.getWidth();
    }
}
