package lab1;

import lab1.adapter.SquarePegAdapter;
import lab1.roundpeg.RoundPeg;
import lab1.socket.RoundSocket;
import lab1.squarepeg.SquarePeg;

public class Launch {
    public static void main(String[] args) {
        var socket = new RoundSocket(10.5);
        var round1 = new RoundPeg(4.7);
        var round2 = new RoundPeg(27);
        System.out.println("Suitable diameter of round peg 1? - " + socket.isSuitableDiameter(round1));
        System.out.println("Suitable diameter of round peg 2? - " + socket.isSuitableDiameter(round2));

        var square1 = new SquarePeg(2.22222222222);
        var square2 = new SquarePeg(11);
        var square1Adapter = new SquarePegAdapter(square1);
        var square2Adapter = new SquarePegAdapter(square2);
        System.out.println("Suitable diameter of square peg 1? - " + socket.isSuitableDiameter(square1Adapter));
        System.out.println("Suitable diameter of square peg 2? - " + socket.isSuitableDiameter(square2Adapter));
    }
}
