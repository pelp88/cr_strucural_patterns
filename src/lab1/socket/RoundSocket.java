package lab1.socket;

import lab1.roundpeg.RoundPeg;

public class RoundSocket {
    private double radius;

    public RoundSocket(double radius) {
        this.radius = radius;
    }

    public boolean isSuitableDiameter(RoundPeg peg) {
        return peg.getRadius() < this.radius;
    }
}
