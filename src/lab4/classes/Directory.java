package lab4.classes;

import java.util.ArrayList;
import java.util.List;

import lab4.interfaces.FilesystemItem;

public class Directory implements FilesystemItem {
    private String path;
    private String name;
    private List<FilesystemItem> contents = new ArrayList<>();

    public Directory(String path, String name){
        this.path = path;
        this.name = name;
    }

    @Override
    public String toString() {
        return this.path + " - " + this.name
            + " - Contents: " + contents;
    }

    public List<FilesystemItem> getContents() {
        return contents;
    }

    public void addContent(FilesystemItem item) {
        this.contents.add(item);
    }

    public void displayContents() {
        System.out.println("Contents of directory " 
            + this.name + ":\n" + this.contents);
    }

    @Override
    public void delete() {
        System.out.println("Directory " + this.path + " deleted");
    }

    @Override
    public void rename(String newName) {
        this.name = newName;
        System.out.println("Directory " + this.path + " renamed to " + newName);
    }

    @Override
    public void info() {
        System.out.println("Info of Directory " 
            + this.name + ":\n" + "\tPath: " 
            + this.path + "\n\tFiles count: " 
            + this.contents.size());
    }
}
