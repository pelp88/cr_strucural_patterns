package lab4.classes;

import lab4.interfaces.FilesystemItem;

public class File implements FilesystemItem {
    private String path;
    private String name;

    public File(String path, String name){
        this.path = path;
        this.name = name;
    }

    @Override
    public String toString() {
        return this.path + " - " + this.name;
    }

    @Override
    public void delete() {
        System.out.println("File " + this.path + " deleted");
    }

    @Override
    public void rename(String newName) {
        this.name = newName;
        System.out.println("File " + this.path + " renamed to " + newName);
    }

    @Override
    public void info() {
        System.out.println("Info of file " 
            + this.name + ":\n" + "\tPath: " + this.path);
    }
}
