package lab4;

import lab4.classes.Directory;
import lab4.classes.File;

public class Launch {
    public static void main(String[] args) {
        var rootDir = new Directory("/", "Root");
        var home = new Directory("/home", "Home");
        var user = new Directory("/home/user", "User home directory");
        var boot = new Directory("/boot", "Boot");

        var kernel = new File("/boot/kernel", "OS Kernel");
        var loader = new File("/boot/loader", "Kernel loader");
        var sysConf = new File("/sys.conf", "System configuration");
        var userFile = new File("/home/user/movie.avi", "Video file");

        rootDir.addContent(home);
        rootDir.addContent(sysConf);
        rootDir.addContent(boot);

        home.addContent(user);

        user.addContent(userFile);

        boot.addContent(kernel);
        boot.addContent(loader);

        rootDir.info();
        System.out.println(rootDir.getContents());
    }
}
