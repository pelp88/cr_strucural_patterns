package lab4.interfaces;

public interface FilesystemItem {
    public void delete();
    public void rename(String newName);
    public void info();
}
